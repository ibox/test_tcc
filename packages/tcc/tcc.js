/**
 * Created by ibox on 9/21/14.
 */
Tcc = {}; //objet de base

var tccObj = {
    api: "2c6e585e59d162bc771d71227548473b740502e06689c83bb3012c83db7aed2d",
    login: "ibox@laposte.net",
    tokenTemp: "",
    ccyTemp: "GBP",
    urlAPI: "https://devapi.thecurrencycloud.com/api/en/v1.0/",
    destination_country_code: "GB"
}

var coreObj = {
    api: "2c6e585e59d162bc771d71227548473b740502e06689c83bb3012c83db7aed2d",
    login: "ibox",
    tokenTemp: "dde77d85-b95a-4eb7-b417-60d12f758edf",
    ccyTemp: "EUR",
    urlAPI: "http://test.hevon.info/api/v1.0/",
    destination_country_code: "GB"
}

//remplacer par bdd later
var EUR_incomingAccount = {
    "nickname": "EUR incoming account",
    "acct_ccy": "EUR",
    "beneficiary_name": "Hevon France",
    //"bank_name": "Societe Generale",
    "bank_address": "6 rue de Lyon 75012 PARIS, FRANCE",
    "acct_number": "00050005611",
    //"sort_code": "30003",
    "iban": "FR7630003015840005000561123",
    "bic_swift": "SOGEFRPP",
    "destination_country_code": "FR",
    "beneficiary_type": "company",
    "beneficiary_company_name": "Hevon",
    "beneficiary_address": "1 rue Tollet",
    "beneficiary_city": "Paris",
    "beneficiary_postcode": "75001",
    "beneficiary_state_or_province": "Ile de France",
    "beneficiary_country": "FR",
    "is_beneficiary": "true",
    "is_source": "false"
}

var EUR_outgoingAccount = {
    "nickname": "EUR outgoing account",
    "acct_ccy": "EUR",
    "beneficiary_name": "Hevon France",
    "bank_address": "6 rue de Lyon 75012 PARIS, FRANCE",
    "acct_number": "00050005611",
    //"sort_code": "30003",
    "iban": "FR76 3000 3015 8400 0500 0561 123",
    "bic_swift": "SOGEFRPP",
    "payment_type": "local",
    "destination_country_code": "FR",
    "is_beneficiary": "false",
    "is_source": "true"
}

var GBP_incomingAccount = {
    "nickname": "GBP incoming account",
    "acct_ccy": "GBP",
    "beneficiary_name": "Hevon UK",
    "bank_address": "1 Churchill Place, London, E14 5HP, UK",
    "acct_number": "13071472",
    "sort_code": "200605", //bank code
    "destination_country_code": "GB",
    "is_beneficiary": "true",
    "is_source": "false"
}

var GBP_outgoingAccount = {
    "nickname": "GBP outgoing account",
    "acct_ccy": "GBP",
    "beneficiary_name": "Hevon UK",
    "bank_address": "1 Churchill Place, London, E14 5HP, UK",
    "acct_number": "00050005612",
    "sort_code": "200605", //bank code
    "destination_country_code": "GB",
    "is_beneficiary": "false",
    "is_source": "true"
}

Tcc.authenticate = function(/*login_id, api_key*/){

//###   equivalent curl à faire en API server   ###
//curl -d "login_id=ibox@laposte.net&api_key=2c6e585e59d162bc771d71227548473b740502e06689c83bb3012c83db7aed2d" https://devapi.thecurrencycloud.com/api/en/v1.0/authentication/token/new

    var tccToken = Meteor.http.post(
            tccObj.urlAPI+"authentication/token/new",
        {
            //timeout: 5000,
            params:{
                //"format": "json",
                "login_id": tccObj.login,
                "api_key": tccObj.api
            }
        }
    );

    if(tccToken.data.status == 'success'){
        console.log('Authentication ok, token stored');
        tccObj.tokenTemp = tccToken.data.data
        return tccToken}
    else{
        console.log('authenticate error : '+ result.message);
        return result.message}

}

Tcc.benificiariesRequiredFields = function(token,tabTemp){

    var result = Meteor.http.get(
            tccObj.urlAPI+token+"/beneficiaries/required_fields",
        {
            params:{
                "ccy": tabTemp.acct_ccy,
                "destination_country_code": tabTemp.destination_country_code
            }
        }
    );

    if(result.data.status == 'success'){
        for(i=0;i<result.data.data.length;i++){
            console.log('Required fields : ' + result.data.data[i].required);
        }
        return result}
    else{
        console.log('benificiariesRequiredFields error : ' + result.message);
        return result.message}

}

Tcc.benificiaryValidateDetails = function(tabTemp){

    var result = Meteor.http.get(
            tccObj.urlAPI+tccObj.tokenTemp+"/beneficiary/validate_details",
        {
            params:{
                "nickname": tabTemp.nickname,
                "acct_ccy": tabTemp.acct_ccy,
                "beneficiary_name": tabTemp.beneficiary_name,
                "acct_number": tabTemp.acct_number,
                //"sort_code": tabTemp.sort_code,
                "iban": tabTemp.iban,
                "bic_swift": tabTemp.bic_swift,
                "destination_country_code": tabTemp.destination_country_code,
                "is_beneficiary": tabTemp.is_beneficiary,
                "is_source": tabTemp.is_source
            }
        }
    );

    if(result.data.status == 'success'){
        console.log('benificiaryValidateDetails ok')
        for(var temp in result.data.data){
            console.log('benificiaryValidateDetails : ' + temp + " > " + result.data.data[temp]);
        }
        return result
    }
    else{
        console.log('benificiaryValidateDetails error : ' + result.message);
        return result.message}

}

Tcc.newBankAccount = function(tabTemp){

    var result = Meteor.http.post(
            tccObj.urlAPI+tccObj.tokenTemp+"/beneficiary/new",
        {
            params:{
                "nickname": tabTemp.nickname,
                "acct_ccy": tabTemp.acct_ccy,
                "beneficiary_name": tabTemp.beneficiary_name,
                "acct_number": tabTemp.acct_number,
                //"sort_code": tabTemp.sort_code,
                "iban": tabTemp.iban,
                "bic_swift": tabTemp.bic_swift,
                "destination_country_code": tabTemp.destination_country_code,
                "beneficiary_type": tabTemp.beneficiary_type,
                "beneficiary_company_name": tabTemp.beneficiary_company_name,
                "beneficiary_address": tabTemp.beneficiary_address,
                "beneficiary_city": tabTemp.beneficiary_city,
                "beneficiary_postcode": tabTemp.beneficiary_postcode,
                "beneficiary_state_or_province": tabTemp.beneficiary_state_or_province,
                "beneficiary_country": tabTemp.beneficiary_country,
                "is_beneficiary": tabTemp.is_beneficiary,
                "is_source": tabTemp.is_source
            }
        }
    );

    if(result.data.status == 'success'){
        console.log('newBankAccount ok')
        for(var temp in result.data.data){
            console.log('newBankAccount : ' + temp + " > " + result.data.data[temp]);
        }
        return result
    }
    else{
        console.log('newBankAccount error : ' + result.message);
        return result.message}

}

Tcc.beneficiaries = function( argsAccount){

    var result = Meteor.http.get(
            "https://devapi.thecurrencycloud.com/api/en/v1.0/"+tccObj.tokenTemp+"/beneficiaries"
    );


    if(result.data.status == 'success'){
        console.log('beneficiaries ok ' + result.data.data.length)
        for(var temp in result.data.data){
            console.log('beneficiaries : ' + temp + " > " + result.data.data[temp]);
            for(var temp2 in result.data.data[temp]){
                console.log('beneficiaries detail: ' + temp2 + " > " + result.data.data[temp][temp2]);
            }
        }
        return result
    }
    else{
        console.log('beneficiaries error : ' + result.message);
        return result.message}

}

Tcc.coreToken = function(/*TEST HEVON API*/){
    var result = Meteor.http.get(
            coreObj.urlAPI + coreObj.tokenTemp + "/calcul"
    );
    console.log('Calling : ' + coreObj.urlAPI + coreObj.tokenTemp + "/calcul");


    if(result.data.status == 'success'){
        console.log('Authentication ok, token stored');
        tccObj.tokenTemp = result.data.data;
        return result}
    else{
        console.log('authenticate error : '+ result.message);
        return result.message}

}

Meteor.methods({
    'getAuthenticate': function(){
        return Tcc.authenticate();
    },
    'getBenificiariesRequiredFields': function(){
        //après le token sera stocké dans la BDD de manière temporaire
        //ensuite en le mettant dans une variable globale côté serveur
        //on economisera les appels Authenticate()
        return Tcc.benificiariesRequiredFields(Tcc.authenticate(),EUR_incomingAccount);

    },
    'getBenificiaryValidateDetails': function(){
        Tcc.authenticate();
        return Tcc.benificiaryValidateDetails(EUR_incomingAccount);
    },
    'getNewBankAccount': function(){ //later mettre passage de token entre functions
        var temp = Tcc.getBenificiaryValidateDetails();
        if(temp.data.status == "success")
            return Tcc.newBankAccount(EUR_incomingAccount);
        else{
            console.log('New Bank Account KO')
            return 'New Bank Account KO'
        }
    },
    'getBeneficiaries': function(){ //later mettre passage de token entre functions
        var temp = Tcc.authenticate();
        if(temp.data.status == "success")
            return Tcc.beneficiaries();
        else{
            console.log('beneficiaries KO')
            return 'beneficiaries KO'
        }
    },
    'getCoreToken': function(){
        return Tcc.coreToken();
    }
})

//### DEV NOTES

// to check JSON structure
// console.log('test ' + JSON.stringify(result, null, 4));


